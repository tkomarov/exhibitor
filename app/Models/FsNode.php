<?php

namespace App\Models;

use Carbon\Carbon;

class FsNode
{
    public $name;
    public $icon;
    public $path;
    public $url;
    public $type;
    public $size;
    public $lastModified;

    public function __construct(array $data)
    {
        $this->name = $data['name'];
        $this->path = $data['path'];
        $this->url = $data['url'];
        $this->type = $data['type'];
        $this->size = $data['size'] ? $this->sizeForHumans($data['size']) : null;
        $this->lastModified = $this->dateForHumans($data['lastModified']);
        $this->icon = $this->iconFor($data['type'], $data['name']);
    }

    protected function sizeForHumans($bytes): string
    {
        $units = [
            1000 => 'KB',
            1000 * 1000 => 'MB',
            1000 * 1000 * 1000 => 'GB',
        ];
        for ($divisor = 1000 * 1000 * 1000; $divisor > 1; $divisor /= 1000) {
            if ($bytes > $divisor) {
                return number_format($bytes / $divisor, 1) . ' ' . $units[$divisor];
            }
        }

        return number_format($bytes, 0) . ' B';
    }

    protected function dateForHumans($timestamp): string
    {
        return Carbon::createFromTimestamp($timestamp)->diffForHumans();
    }

    protected function iconFor($type, $name)
    {
        if ($type === 'directory') {
            return 'regular/folder';
        }

        switch (strtolower(pathinfo($name)['extension'] ?? '')) {
            case 'jpeg':
            case 'jpg':
            case 'png':
            case 'svg':
            case 'gif':
            case 'tiff':
                return 'regular/image';
            case 'mp3':
            case 'm4a':
            case 'aac':
            case 'wav':
            case 'ogg':
                return 'regular/file-audio';
            case 'mkv':
            case 'mp4':
            case 'avi':
            case 'wmv':
            case 'webm':
                return 'regular/file-video';
            case 'pdf':
                return 'regular/file-pdf';
            case 'txt':
            case 'md':
                return 'regular/file-alt';
            case 'zip':
            case '7z':
            case 'rar':
                return 'regular/file-archive';
            default:
                return 'regular/file';
        }
    }
}
