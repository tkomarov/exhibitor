<?php

namespace App\Services;

use Illuminate\Support\Str;

class Slash
{
    public function start(string $string): string
    {
        return Str::startsWith($string, '/') ? $string : '/' . $string;
    }

    public function end(string $string): string
    {
        return Str::endsWith($string, '/') ? $string : $string . '/';
    }

    public function dontStart(string $string): string
    {
        return Str::startsWith($string, '/') ? ltrim($string, '/') : $string;
    }

    public function dontEnd(string $string): string
    {
        return Str::endsWith($string, '/') ? rtrim($string, '/') : $string;
    }

    public function trim(string $string): string
    {
        return trim($string, '/');
    }

    public function wrap(string $string): string
    {
        return $this->start($this->end($string));
    }
}
