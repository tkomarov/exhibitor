<?php

namespace App\Services;

use App\Models\FsNode;
use Illuminate\Contracts\Filesystem\Filesystem;
use Illuminate\Support\Str;

class DirectoryScanner
{
    protected $storage;
    protected $slash;

    public function __construct(Filesystem $storage, Slash $slash)
    {
        $this->storage = $storage;
        $this->slash = $slash;
    }

    public function scan($dir = '/')
    {
        $directories = collect($this->storage->directories($dir))->map(function ($directory) use ($dir) {
            $directory = $this->slash->start($directory);
            return new FsNode([
                'url' => '/browse?path=' . urlencode($directory),
                'path' => $directory,
                'name' => $this->cleanName($directory, $dir),
                'size' => null, // $this->storage->size($directory),
                'lastModified' => $this->storage->lastModified($directory),
                'type' => 'directory',
            ]);
        });
        $files = collect($this->storage->files($dir))->map(function ($file) use ($dir) {
            $file = $this->slash->start($file);
            return new FsNode([
                'url' => '/download?path=' . urlencode($file),
                'path' => $file,
                'name' => $this->cleanName($file, $dir),
                'size' => $this->storage->size($file),
                'lastModified' => $this->storage->lastModified($file),
                'type' => 'file',
            ]);
        });

        return $directories->merge($files);
    }

    protected function cleanName(string $name, string $dir = '/'): string
    {
        return $this->slash->dontStart(
            Str::substr($name, mb_strlen(
                $this->slash->start($dir)
            ))
        );
    }
}
