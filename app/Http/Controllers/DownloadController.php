<?php

namespace App\Http\Controllers;

use App\Services\Slash;
use Illuminate\Contracts\Filesystem\Filesystem;
use Illuminate\Http\Request;

class DownloadController extends Controller
{
    public function download(Request $request, Filesystem $storage, Slash $slash)
    {
        $path = $request->validate(['path' => 'required|string'])['path'];

        if (!$storage->exists($path)) {
            abort(404, 'File not found');
        }

        $storagePath = $storage->getDriver()->getAdapter()->getPathPrefix();
        $fullpath = $slash->end($storagePath) . $path;
        $filename = pathinfo($fullpath)['basename'];

        return response()->download($fullpath, $filename, [
            'Content-Type' => mime_content_type($fullpath),
        ]);
    }
}
