<?php

namespace App\Http\Controllers;

use App\Services\DirectoryScanner;
use Illuminate\Http\Request;

class BrowseController extends Controller
{
    public function index(DirectoryScanner $directoryScanner, Request $request) {
        return view('browse', [
            'appData' => [
                'items' => $directoryScanner->scan($path = $request->input('path', '/')),
                'path' => $path,
            ],
        ]);
    }
}
