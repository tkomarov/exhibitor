import Vue from 'vue';
import ExhibitorApp from "./components/ExhibitorApp";

import 'vue-awesome/icons/regular/folder';
import 'vue-awesome/icons/regular/file';
import 'vue-awesome/icons/regular/image';
import 'vue-awesome/icons/regular/file-audio';
import 'vue-awesome/icons/regular/file-video';
import 'vue-awesome/icons/regular/file-pdf';
import 'vue-awesome/icons/regular/file-alt';
import 'vue-awesome/icons/regular/file-archive';
import Icon from 'vue-awesome/components/Icon';

Vue.component('VIcon', Icon);


if (document.getElementById('app')) {
    new Vue({
        el: '#app',

        components: {
            ExhibitorApp,
        },

        template: `<ExhibitorApp/>`
    })
}

window.toggleDarkMode = function (value = null) {
    if (value === null) {
        value = !document.getElementById('thebody').classList.contains('dark');
    }

    value ? setDarkMode() : unsetDarkMode();
}

window.setDarkMode = function () {
    localStorage.setItem('exhibitor.darkMode', 'true');
    document.getElementById('thebody').classList.add('dark');
}
window.unsetDarkMode = function () {
    localStorage.setItem('exhibitor.darkMode', 'false');
    document.getElementById('thebody').classList.remove('dark');
}

document.onreadystatechange = () => {
    if (document.readyState === 'complete') {
        let darkMode = localStorage.getItem('exhibitor.darkMode');
        if (darkMode) {
            darkMode = JSON.parse(darkMode);
        } else {
            darkMode = true;
        }
        toggleDarkMode(darkMode);
    }
};
