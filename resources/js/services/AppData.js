class AppData {
    constructor() {
        Object.assign(this, window.$appData || {});
    }
}

export default new AppData;
