<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ config('app.name') }}</title>

    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
</head>
<body id="thebody" class="antialiased dark">

<div class="container mt-8">

    @yield('content')

    <div class="text-sm mt-2 text-gray-400 dark:text-gray-600 flex flex-row justify-between">
        <div>
            Exhibitor v{{ \App\Exhibitor::VERSION }} | &copy; {{ date('Y') }}
        </div>
        <div>
            <a role="button" onclick="toggleDarkMode()">Toggle Dark Mode</a>
            @auth
                |
                <form class="inline" action="/logout" method="POST">
                    @csrf
                    <button type="submit">Log out</button>
                </form>
            @endauth
        </div>
    </div>
</div>

@stack('scripts')
<script src="{{ mix('js/app.js') }}"></script>
</body>
</html>
