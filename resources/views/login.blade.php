@extends('template')

@section('content')
    <div class="auth-form p-8 flex flex-col items-center justify-center">
        @if ($errors->any())
            <div class="p-10 mb-4 bg-red-300 dark:bg-red-700 text-red-900 dark:text-red-300">
                <ul class="list-disc">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="w-4/5 sm:w-3/4 lg:w-1/2">
            <form action="/login" method="POST">
                @csrf
                <div class="form-input">
                    <label for="input-email" class="sr-only">Email</label>
                    <input id="input-email" type="email" name="email" placeholder="Email" value="{{ old('email') }}">
                </div>
                <div class="form-input">
                    <label for="input-password" class="sr-only">Password</label>
                    <input id="input-password" type="password" name="password" placeholder="Password">
                </div>
                <button class="btn block w-full" type="submit">Log in</button>
            </form>
        </div>
    </div>
@endsection
