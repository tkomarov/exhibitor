@extends('template')

@section('content')
    <div id="app"></div>
@endsection

@push('scripts')
    <script>
        window.$appData = @json($appData);
    </script>
@endpush
