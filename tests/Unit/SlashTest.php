<?php

namespace Tests\Unit;

use App\Services\Slash;
use PHPUnit\Framework\TestCase;

class SlashTest extends TestCase
{
    protected $slash;

    protected function setUp(): void {
        parent::setUp();

        $this->slash = new Slash;
    }

    /**
     * @test
     */
    public function it_appends_slash_to_string_if_it_does_not_already_end_with_one()
    {
        $this->assertEquals('foo/', $this->slash->end('foo'));
        $this->assertEquals('/foo/', $this->slash->end('/foo'));
        $this->assertEquals('foo/', $this->slash->end('foo/'));
    }

    /**
     * @test
     */
    public function it_prepends_slash_to_string_if_it_does_not_already_start_with_one()
    {
        $this->assertEquals('/foo', $this->slash->start('foo'));
        $this->assertEquals('/foo/', $this->slash->start('foo/'));
        $this->assertEquals('/foo', $this->slash->start('/foo'));
    }

    /**
     * @test
     */
    public function it_removes_starting_slashes_from_string()
    {
        $this->assertEquals('foo', $this->slash->dontStart('/foo'));
        $this->assertEquals('foo', $this->slash->dontStart('////foo'));
        $this->assertEquals('foo/', $this->slash->dontStart('/foo/'));
    }

    /**
     * @test
     */
    public function it_removes_ending_slashes_from_string()
    {
        $this->assertEquals('foo', $this->slash->dontEnd('foo/'));
        $this->assertEquals('foo', $this->slash->dontEnd('foo////'));
        $this->assertEquals('/foo', $this->slash->dontEnd('/foo/'));
    }

    /**
     * @test
     */
    public function it_trims_slashes_from_both_sides_of_string()
    {
        $this->assertEquals('foo', $this->slash->trim('/foo/'));
        $this->assertEquals('foo', $this->slash->trim('////foo//////'));
        $this->assertEquals('foo', $this->slash->trim('foo/'));
        $this->assertEquals('foo', $this->slash->trim('/foo'));
        $this->assertEquals('foo', $this->slash->trim('foo'));
    }

    /**
     * @test
     */
    public function it_wraps_string_with_slashes_on_both_sides()
    {
        $this->assertEquals('/foo/', $this->slash->wrap('foo'));
        $this->assertEquals('/foo/', $this->slash->wrap('/foo'));
        $this->assertEquals('/foo/', $this->slash->wrap('foo/'));
        $this->assertEquals('/foo/', $this->slash->wrap('/foo/'));
    }
}
