<?php

namespace Tests\Feature;

use App\Services\DirectoryScanner;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Tests\TestCase;

class DirectoryScannerTest extends TestCase
{
    /**
     * @test
     */
    public function it_returns_a_list_of_files_and_directories_in_given_path()
    {
        $disk = Storage::fake('exhibitor');

        $subdirectory = 'Subdirectory with more files';

        // make some directories
        foreach ($directories = ['foo', 'bar', 'some.dir', $subdirectory] as $dir) {
            $disk->makeDirectory($dir);
        }
        // add some files
        foreach ($files = ['foo.jpg', 'bar.txt', '1234.html', 'no_extension'] as $file) {
            $disk->put($file, $this->randomData());
        }
        // add some files to a subdirectory
        foreach ($files as $file) {
            $disk->put("$subdirectory/$file", $this->randomData());
        }

        $expectedRootContents = collect($directories)->merge($files);
        $expectedSubDirContents = collect($files);

        $scanner = $this->directoryScanner();

        $rootContents = collect($scanner->scan('/'));
        $this->assertEquals($expectedRootContents->count(), $rootContents->count());
        foreach ($expectedRootContents as $item) {
            $this->assertContains($item, $rootContents->map->name);
        }

        $subDirContents = collect($scanner->scan($subdirectory));
        $this->assertEquals($expectedSubDirContents->count(), $subDirContents->count());
        foreach ($expectedSubDirContents as $item) {
            $this->assertContains($item, $subDirContents->map->name);
        }
    }

    protected function directoryScanner(): DirectoryScanner
    {
        return app()->make(DirectoryScanner::class);
    }

    protected function randomData()
    {
        return Str::random(mt_rand(10, 500));
    }
}
