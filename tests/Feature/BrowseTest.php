<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Tests\TestCase;

class BrowseTest extends TestCase
{
    /**
     * @test
     */
    public function it_is_protected_by_auth()
    {
        $this->get('/browse')->assertRedirect('/login');
    }

    /**
     * @test
     */
    public function it_can_load_browse_endpoint()
    {
        $response = $this
            ->actingAs(User::factory()->create())
            ->get('/browse');

        $response->assertStatus(200);
    }

    /**
     * @test
     */
    public function it_returns_a_list_of_files_and_directories()
    {
        $disk = Storage::fake('exhibitor');
        foreach ($files = ['foo.txt', 'bar.html', 'no_extension'] as $file) {
            $disk->put($file, Str::random());
        }
        $disk->put('subdirectory/test.txt', Str::random());

        $test = $this->actingAs(User::factory()->create())->get('/browse');

        foreach (array_merge(['subdirectory'], $files) as $item) {
            $test->assertSee($item);
        }

        $this->get('/browse?path=' . urlencode('/subdirectory'))->assertSee('test.txt');
    }

    /**
     * @test
     */
    public function it_redirects_root_route_to_browse()
    {
        $this->get('/')->assertRedirect('/browse');
    }
}
