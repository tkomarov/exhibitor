<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class DownloadTest extends TestCase
{
    /**
     * @test
     */
    public function it_serves_up_files_for_download()
    {
        $disk = Storage::fake('exhibitor');
        $disk->put('directory/file.txt', 'This is a file');

        $this->actingAs(User::factory()->create())
            ->get('download?path=' . urlencode('directory/file.txt'))
            ->assertDownload('file.txt');
    }
}
